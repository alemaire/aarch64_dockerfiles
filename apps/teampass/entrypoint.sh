#!/bin/bash
if [ ! -d ${VOL}/includes/config ];
then
	echo "Initial setup..."
  cd ${VOL}/..
	wget ${TEAMPASS_ARCHIVE}/${TEAMPASS_VERSION}.tar.gz
  tar -xf $TEAMPASS_VERSION.tar.gz && mv TeamPass-${TEAMPASS_VERSION} ${VOL}
	mkdir ${VOL}/sk
fi

if [ -f ${VOL}/includes/config/settings.php ] ;
then
	echo "Teampass is ready."
	rm -rf ${VOL}/install
else
	echo "Teampass is not configured yet. Open it in a web browser to run the install process."
	echo "Use ${VOL}/sk for the absolute path of your saltkey."
	echo "When setup is complete, restart this image to remove the install directory."
fi

php-fpm7 -F
