FROM yen3/binfmt-register as builder
FROM arm64v8/alpine:3.8
COPY --from=builder /qemu/qemu-aarch64-static /usr/local/bin/qemu-aarch64-static
LABEL maintainer="Shaarli Community"
WORKDIR /var/www
RUN apk --update --no-cache add \
        ca-certificates nginx s6 \
        php7 php7-ctype php7-curl php7-fpm php7-gd php7-iconv php7-intl \
        php7-json php7-mbstring php7-openssl php7-session php7-xml php7-zlib \
    && rm -rf /etc/php7/php-fpm.d/www.conf \
    && sed -i 's/post_max_size.*/post_max_size = 10M/' /etc/php7/php.ini \
    && sed -i 's/upload_max_filesize.*/upload_max_filesize = 10M/' /etc/php7/php.ini \
    && ln -sf /dev/stdout /var/log/nginx/shaarli.access.log \
    && ln -sf /dev/stderr /var/log/nginx/shaarli.error.log \
    && deluser nginx && addgroup -g 1000 nginx \
    && adduser -D -G nginx -u 1000 -h /var/lib/nginx -s /sbin/nologin nginx

ARG SHAARLI_VERSION
ARG SHAARLI_RELEASE=https://github.com/shaarli/Shaarli/releases/download
ADD ${SHAARLI_RELEASE}/${SHAARLI_VERSION}/shaarli-${SHAARLI_VERSION}-full.tar.gz /var/www/
COPY .docker/nginx.conf /etc/nginx/nginx.conf
COPY .docker/php-fpm.conf /etc/php7/php-fpm.conf
COPY .docker/services.d /etc/services.d
RUN tar -xzf /var/www/shaarli-${SHAARLI_VERSION}-full.tar.gz \
    && rm /var/www/shaarli-${SHAARLI_VERSION}-full.tar.gz \
    && chown -R nginx:nginx . /var/tmp/nginx \
    && mv /var/www/Shaarli /var/www/shaarli \
    && rm -f /usr/local/bin/qemu-aarch64-static
VOLUME /var/www/shaarli/cache
VOLUME /var/www/shaarli/data

EXPOSE 80

ENTRYPOINT ["/bin/s6-svscan", "/etc/services.d"]
CMD []
