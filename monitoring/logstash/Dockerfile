FROM yen3/binfmt-register:0.1 as binfmt
#Latest version of node tested on.
FROM arm64v8/alpine:3.9 AS dist
COPY --from=binfmt /qemu/qemu-aarch64-static /usr/local/bin/qemu-aarch64-static

ENV DUMB_INIT_VERSION=1.2.2-r1

ENV SU_EXEC_VERSION=0.2-r0

ARG LOGSTASH_VERSION
# Do not split this into multiple RUN!
# Docker creates a layer for every RUN-Statement
# therefore an 'apk delete' has no effect
COPY entrypoint.sh /bin/

RUN mkdir -p /opt \
 && mkdir -p /logstash/etc \
 && apk update \
 && apk upgrade \
 && apk add --no-cache \
        ca-certificates curl \
        openjdk8-jre-base \
        su-exec==${SU_EXEC_VERSION} \
        dumb-init==${DUMB_INIT_VERSION} \
 && update-ca-certificates \
 && curl -Lo logstash-oss.tgz https://artifacts.elastic.co/downloads/logstash/logstash-oss-$LOGSTASH_VERSION.tar.gz \
 && tar -xf logstash-oss.tgz -C /opt/ \
 && ln -s /opt/logstash-$LOGSTASH_VERSION /opt/logstash \
 && chmod +x /bin/entrypoint.sh


ENV LOGSTASH_OPT=""
ENV LOGSTASH_CONF="logstash.conf"

ENV LD_PRELOAD=""
ENV DUMB_INIT_SETSID 0
EXPOSE 24224 5140

ENTRYPOINT ["/bin/entrypoint.sh"]

CMD exec /opt/logstash/bin/logstash -f /logstash/etc/${LOGSTASH_CONF} $LOGSTASH_OPT
