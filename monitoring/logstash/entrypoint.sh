#!/usr/bin/dumb-init /bin/sh

uid=${DOCKER_UID:-1000}

# check if a old docker-user user exists and delete it
cat /etc/passwd | grep docker-user
if [ $? -eq 0 ]; then
    deluser docker-user
fi

# (re)add the container user with $DOCKER_UID
adduser -D -g '' -u ${uid} -h /home/docker-user docker-user

#source vars if file exists
DEFAULT=/etc/default/docker-user

if [ -r $DEFAULT ]; then
    set -o allexport
    source $DEFAULT
    set +o allexport
fi

# chown home and data folder
chown -R docker-user /home/docker-user

exec su-exec docker-user "$@"
