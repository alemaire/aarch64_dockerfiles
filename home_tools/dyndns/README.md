# docker-curl-dyndns

## Usage
Simple run the following command:
```
docker run --name "dyndns" -e INTERVAL_TIME="300" -e PASSWORD=my_dyndns_password -e USERNAME=my_dyndns_user -e DYN_DNS_URL="https://example.com"
curl-dyndns
```

or use docker-compose:
```
version: '2'
services:
  dyndns:
    container_name: dyndns
    image: curl-dyndns
    restart: always
    environment:
    - DYN_DNS_URL=https://example.com
    - INTERVAL_TIME=300
```

## Parameters

### DYN_DNS_URL (required):
The URL which get triggered by wget.

### INTERVAL_TIME (optional):
Interval time between checks of changes of the external ip address in seconds. Default is 300.

### CURL_PARAM (optional):
Optional parameters for wget (no output will be logged!).
