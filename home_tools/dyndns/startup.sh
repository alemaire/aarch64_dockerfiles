#!/bin/bash

# Check if INTERVAL_TIME is set
if [ -z "$INTERVAL_TIME" ]; then
  echo "Variable INTERVAL_TIME is set to 300"
  INTERVAL_TIME=300
fi

old_ip=""
# Infinite loop
#curl ifconfig.me
#curl icanhazip.com
#curl ipecho.net/plain
#curl ifconfig.co
while true; do
  current_ip=$(curl -s https://canihazip.com/s)
  if [ "$current_ip" = "" ]; then
    current_ip=$(curl -s https://ifconfig.co)
    if [ "$current_ip" = "" ]; then
      current_ip=$(curl -s https://ipecho.net/plain)
    fi
  fi
  if ! [ "$current_ip" = "$old_ip" ]; then
    curl -s -u $USERNAME:$PASSWORD "http://www.ovh.com/nic/update?system=dyndns&hostname=$HOSTNAME&myip=$current_ip"
    old_ip="$current_ip"
    echo -n "DynDNS is updated to $current_ip on "; date
  fi
  sleep $INTERVAL_TIME
done
